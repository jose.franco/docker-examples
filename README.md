# docker-examples

La idea de este repositorio es tener una base de los dockerfile's que mas usamos en los distintos proyectos.

### Tipos de dockerfiles

  - **Dockerfile**: TBD
  - **Dockerfile.multistage**: TBD 
  - **Dockerfile.volume**: TBD

Para net-core:
  - **Dockerfile.alpine**: TBD


### Lenguajes disponibes

A continuación se muestra los lenguajes que estamos usando

* [Go] - TBD
* [NetCore] - TBD


### Referencias

[Optimizing ASP.NET Core Docker Image size]: <https://www.hanselman.com/blog/OptimizingASPNETCoreDockerImageSizes.aspx>
[.NET Framework to .NET Core to Docker]: <https://dev.to/magnusstrale/net-framework-to-net-core-to-docker-3m3b>
[How to containerize the .NET Framework web apps with Windows Containers and Docker]: <https://github.com/dotnet-architecture/eShopModernizing/wiki/02.-How-to-containerize-the-.NET-Framework-web-apps-with-Windows-Containers-and-Docker>


License
----

MIT
**Free Software, Hell Yeah!**

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
